import cv2

im = cv2.imread("curretFrame1.png")

im_bw = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

im_blur = cv2.GaussianBlur(im_bw, (9, 9), 2, 2)

circles = cv2.HoughCircles(im_bw, cv2.cv.CV_HOUGH_GRADIENT, 1, minDist=5, param1=50, param2=50, minRadius=1, maxRadius=30)

for i in circles[0,:]:
    cv2.circle(im,(i[0],i[1]),i[2],(0,255,0),2)
    cv2.circle(im,(i[0],i[1]),2,(0,0,255),3)

cv2.imshow("current frame", im)
# cv2.imwrite("result.png", im)
cv2.waitKey(0)
cv2.destroyAllWindows()
