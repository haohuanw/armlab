clear all
clc

speed_cmd = csvread('commandSpeed.csv');

base_speed_cmd = speed_cmd(:,1);
shoulder_speed_cmd = speed_cmd(:,2);
elbow_speed_cmd = speed_cmd(:,3);
wrist_speed_cmd = speed_cmd(:,4);

base_speed = speed_cmd(1:12:end,1);
shoulder_speed = speed_cmd(1:12:end,2);
elbow_speed = speed_cmd(1:12:end,3);
wrist_speed = speed_cmd(1:12:end, 4);

time_cmd = csvread('commandTime.csv');

base_time_cmd = time_cmd(:,1); %- time_fb(1,1);
shoulder_time_cmd = time_cmd(:,2);% - time_fb(1,2);
elbow_time_cmd = time_cmd(:,3); %- time_fb(1,3);
wrist_time_cmd = time_cmd(:,4); %- time_fb(1,4);

base_time = time_cmd(1:12:end,1); %- time_fb(1,1);
shoulder_time = time_cmd(1:12:end,2);% - time_fb(1,2);
elbow_time = time_cmd(1:12:end,3); %- time_fb(1,3);
wrist_time = time_cmd(1:12:end,4); %- time_fb(1,4);

speed_fb = csvread('speedfeedback.csv');
base_speed_fb = speed_fb(:,1);
shoulder_speed_fb = speed_fb(:,2);
elbow_speed_fb = speed_fb(:,3);
wrist_speed_fb = speed_fb(:,4);


time_fb = csvread('timefeedback.csv');
base_time_fb = time_fb(:,1);%-time_fb(1,1);
shoulder_time_fb = time_fb(:,2);%-time_fb(1,2);
elbow_time_fb = time_fb(:,3);%-time_fb(1,3);
wrist_time_fb = time_fb(:,4);%-time_fb(1,4);


fig1 = figure;
figure(fig1)
title('Teach and Repeat with Cubic Spline on Base Joint')
xlabel('timestamp (us)')
ylabel('joint speed (%)')
hold all;
plot(base_time_fb, base_speed_fb,'r');
plot(base_time_cmd, base_speed_cmd,'g');
plot(base_time, base_speed,'b');
legend('Base Actual', 'Base Command', 'Base Waypoints')
hold off;

fig2 = figure;
figure(fig2)
title('Teach and Repeat with Cubic Spline on Shoulder Joint')
xlabel('timestamp (us)')
ylabel('joint speed (%)')
hold all;
plot(shoulder_time_fb, shoulder_speed_fb,'r');
plot(shoulder_time_cmd, shoulder_speed_cmd,'g');
plot(shoulder_time, shoulder_speed,'b');
legend('Shoulder Actual', 'Shoulder Command', 'Shoulder Waypoints')
hold off;

fig3 = figure;
figure(fig3)
title('Teach and Repeat with Cubic Spline on Elbow Joint')
xlabel('timestamp (us)')
ylabel('joint speed (%)')
hold all;
plot(elbow_time_fb, elbow_speed_fb,'r');
plot(elbow_time_cmd, elbow_speed_cmd,'g');
plot(elbow_time, elbow_speed,'b');
legend('Elbow Actual', 'Elbow Command', 'Elbow Waypoints')
hold off;

fig4 = figure;
figure(fig4)
title('Teach and Repeat with Cubic Spline on Wrist Joint')
xlabel('timestamp (us)')
ylabel('joint speed (%)')
hold all;
plot(wrist_time_fb, wrist_speed_fb,'r');
plot(wrist_time_cmd, wrist_speed_cmd,'g');
plot(wrist_time, wrist_speed,'b');
legend('Wrist Actual', 'Wrist Command', 'Wrist Waypoints')
hold off;