clear all
clc

joint_cmd = csvread('commandPos.csv');

base_joint_cmd = joint_cmd(:,1);
shoulder_joint_cmd = joint_cmd(:,2);
elbow_joint_cmd = joint_cmd(:,3);
wrist_joint_cmd = joint_cmd(:,4);

base_joint = joint_cmd(1:12:end,1);
shoulder_joint = joint_cmd(1:12:end,2);
elbow_joint = joint_cmd(1:12:end,3);
wrist_joint = joint_cmd(1:12:end, 4);

time_cmd = csvread('commandTime.csv');

base_time_cmd = time_cmd(:,1); %- time_fb(1,1);
shoulder_time_cmd = time_cmd(:,2);% - time_fb(1,2);
elbow_time_cmd = time_cmd(:,3); %- time_fb(1,3);
wrist_time_cmd = time_cmd(:,4); %- time_fb(1,4);

base_time = time_cmd(1:12:end,1); %- time_fb(1,1);
shoulder_time = time_cmd(1:12:end,2);% - time_fb(1,2);
elbow_time = time_cmd(1:12:end,3); %- time_fb(1,3);
wrist_time = time_cmd(1:12:end,4); %- time_fb(1,4);

joint_fb = csvread('jointfeedback.csv');
base_joint_fb = joint_fb(:,1);
shoulder_joint_fb = joint_fb(:,2);
elbow_joint_fb = joint_fb(:,3);
wrist_joint_fb = joint_fb(:,4);


time_fb = csvread('timefeedback.csv');
base_time_fb = time_fb(:,1);%-time_fb(1,1);
shoulder_time_fb = time_fb(:,2);%-time_fb(1,2);
elbow_time_fb = time_fb(:,3);%-time_fb(1,3);
wrist_time_fb = time_fb(:,4);%-time_fb(1,4);


fig1 = figure;
figure(fig1)
title('Teach and Repeat with Cubic Spline on Base Joint')
xlabel('timestamp (us)')
ylabel('joint angle (radians)')
hold all;
plot(base_time_fb, base_joint_fb,'r');
plot(base_time_cmd, base_joint_cmd,'g');
plot(base_time, base_joint,'b');
legend('Base Actual', 'Base Command', 'Base Waypoints')
hold off;

fig2 = figure;
figure(fig2)
title('Teach and Repeat with Cubic Spline on Shoulder Joint')
xlabel('timestamp (us)')
ylabel('joint angle (radians)')
hold all;
plot(shoulder_time_fb, shoulder_joint_fb,'r');
plot(shoulder_time_cmd, shoulder_joint_cmd,'g');
plot(shoulder_time, shoulder_joint,'b');
legend('Shoulder Actual', 'Shoulder Command', 'Shoulder Waypoints')
hold off;

fig3 = figure;
figure(fig3)
title('Teach and Repeat with Cubic Spline on Elbow Joint')
xlabel('timestamp (us)')
ylabel('joint angle (radians)')
hold all;
plot(elbow_time_fb, elbow_joint_fb,'r');
plot(elbow_time_cmd, elbow_joint_cmd,'g');
plot(elbow_time, elbow_joint,'b');
legend('Elbow Actual', 'Elbow Command', 'Elbow Waypoints')
hold off;

fig4 = figure;
figure(fig4)
title('Teach and Repeat with Cubic Spline on Wrist Joint')
xlabel('timestamp (us)')
ylabel('joint angle (radians)')
hold all;
plot(wrist_time_fb, wrist_joint_fb,'r');
plot(wrist_time_cmd, wrist_joint_cmd,'g');
plot(wrist_time, wrist_joint,'b');
legend('Wrist Actual', 'Wrist Command', 'Wrist Waypoints')
hold off;


