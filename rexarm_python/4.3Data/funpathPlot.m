clear all
clc
t = 0:1:12;
X = 150*cos(t);
Y = 150*sin(t);
Z = 5+t*30;
fig = figure;
figure(fig)
title('Fun Path Plot')
xlabel('X-axis (mm)')
ylabel('Y-axis (mm)')
zlabel('Z-axis (mm)')
plot3(X,Y,Z)
uicontrol('Style','text','Position', [200 400 200 20],'String','Fun Path Plot','FontSize',16,'HorizontalAlignment','Center','BackgroundColor',[.8 .8 .8])
rotate3d on;
res = [X;Y;Z]