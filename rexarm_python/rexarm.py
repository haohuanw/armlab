import lcm
import time
import numpy as np
import numpy.linalg as la
import copy
import csv

from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t

PI = np.pi
D2R = PI/180.0
ANGLE_TOL = 2*PI/180.0 

def law_of_cosine_length(L1, L2, angle):
    print "length", L1, L2, angle
    return np.sqrt(L1**2 + L2**2 - 2*L1*L2*np.cos(angle))

#return cosine of angle between L1 and L2
def law_of_cosine_angle(L1, L2, L3):
    print "angle: ", L1, L2, L3
    print "cos: ",(-L3**2 + L1**2 + L2**2) / (2*L1*L2)
    return np.arccos((-L3**2 + L1**2 + L2**2) / (2*L1*L2))


""" Rexarm Class """
class Rexarm():
    def __init__(self):

        """ Commanded Values """
    	""" joint_angles = [base, shoulder, elbow, wrist]"""
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians
        # you SHOULD change this to contorl each joint speed separately 
        self.speed = [0.0, 0.0, 0.0, 0.0]                         # 0 to 1
        self.max_torque = 0.5                    # 0 to 1
        self.clamp_values = [180.0*D2R, 110.0*D2R, 115.0*D2R, 120.0*D2R] # since values are symmetric, use absolute value to compare

        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius               
        self.time_fb = [0.0, 0.0, 0.0, 0.0]         # us
        self.L = [115, 100, 100, 109]
        """ Plan - TO BE USED LATER """
        self.planPos = []
        self.planSpeed = []
        self.plan_status = 0
        self.wpt_number = 0
        self.wpt_total = 0

        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)
        self.save_data = False
	self.jointfp = csv.writer(open("jointfeedback.csv","wb"))	
        self.speedfp = csv.writer(open("speedfeedback.csv", "wb"))
	self.timefp = csv.writer(open("timefeedback.csv","wb"))	
	self.commandPosfp = csv.writer(open("commandPos.csv","wb"))	
	self.commandSpeedfp = csv.writer(open("commandSpeed.csv","wb"))	
        self.commandTimefp = csv.writer(open("commandTime.csv", "wb"))

    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        msg = dynamixel_command_list_t()
        msg.len = 4
        curr_time = []
        self.clamp()
        for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            curr_time.append(cmd.utime)
            cmd.position_radians = self.joint_angles[i]
            # you SHOULD change this to contorl each joint speed separately 
	    if self.speed[i] == 0:
		cmd.speed = 0.1
	    else:
            	cmd.speed = self.speed[i]+0.03
		#cmd.speed = 0.1
            cmd.max_torque = self.max_torque
            #print cmd.position_radians
            msg.commands.append(cmd)
        if self.save_data:
            self.commandPosfp.writerow(self.joint_angles)
            self.commandSpeedfp.writerow(self.speed)
            self.commandTimefp.writerow(curr_time)
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians 
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature
	    self.time_fb[i] = msg.statuses[i].utime
        
        if self.save_data:
            self.timefp.writerow(self.time_fb)
            self.jointfp.writerow(self.joint_angles_fb)
            self.speedfp.writerow(self.speed_fb)
    
    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        TO DO: IMPLEMENT SUCH FUNCTION
        """
        for i in xrange(len(self.joint_angles)):
            if abs(self.joint_angles[i]) > self.clamp_values[i]:
                if self.joint_angles[i] > 0:
                    self.joint_angles[i] = self.clamp_values[i]
                else:
                    self.joint_angles[i] = -self.clamp_values[i]

    def plan_command(self):
        """ Command waypoints - TO BE ADDED LATER """
        if self.plan_status == 0:
	    pass
        elif self.plan_status == 1:
            self.joint_angles = self.planPos[self.wpt_number]
            self.speed = self.planSpeed[self.wpt_number]
	        #print "publish waypoints", self.joint_angles
            self.cmd_publish()
            self.plan_status = 2
        elif self.plan_status == 2:
	        #print "current arm pos", self.joint_angles_fb
	    if self.is_arm_near():
                #time.sleep(0.5)
	        # finish all waypoints
	        if self.wpt_number == (self.wpt_total-1):
	            self.plan_status = 0
                    self.planPos = []
                    self.planSpeed = []
                    self.wpt_number = 0
                    self.wpt_total = 0
	            # increment current wpt_number and go back to status 1
                else:
		    self.wpt_number += 1
		    self.plan_status = 1
            
    def is_arm_near(self):
        arm_len = len(self.joint_angles)
        for i in xrange(arm_len):
            if abs(self.joint_angles_fb[i] - self.joint_angles[i]) > 0.1:
	        return False
	return True

    def rexarm_FK(self, link):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        """
        """
	    theta 	 | alpha | a  | d
	    theta1-90| 90    | 0  | L1
	    90+theta2| 0	 | L2 | 0
	    theta3   | 0     | L3 | 0
            theta4   | 0 	 | L4 | 0
	    """
        alpha = [np.pi/2, 0, 0, 0]
        a = [0, self.L[1], self.L[2], self.L[3]]
        d = [self.L[0], 0, 0, 0]
        #theta = [self.joint_angles_fb[0]-np.pi/2, self.joint_angles_fb[1]+np.pi/2, self.joint_angles_fb[2], self.joint_angles_fb[3]]
        theta = [self.joint_angles_fb[0]+np.pi, self.joint_angles_fb[1]+np.pi/2, self.joint_angles_fb[2], self.joint_angles_fb[3]]
        mat_array = np.array([[1, 0, 0, 0],
	    			 [0, 1, 0, 0],
	    			 [0, 0, 1, 0],
	    			 [0, 0, 0, 1]])	
        result = np.array(np.zeros((4, 4)))
        for i in xrange(4):
	        A = np.array(
	    	[[np.cos(theta[i]), -np.sin(theta[i])*np.cos(alpha[i]), np.sin(theta[i])*np.sin(alpha[i]), a[i]*np.cos(theta[i])],
	             [np.sin(theta[i]), np.cos(theta[i])*np.cos(alpha[i]), -np.cos(theta[i])*np.sin(alpha[i]), a[i]*np.sin(theta[i])],
	             [0            , np.sin(alpha[i])              ,  np.cos(alpha[i])              , d[i]              ],
	             [0            , 0                          ,  0                          , 1                 ]]
        		)
	        mat_array = np.dot(mat_array, A)	
	        result[i] = mat_array[:,3]
	    # calculate psi
        joint_n_prev = result[-2][:3]
        joint_n = result[-1][:3]
        joint_vector = np.subtract(joint_n, joint_n_prev)
        joint_vector = joint_vector / la.norm(joint_vector)
        zero_vector = copy.deepcopy(joint_vector)
        zero_vector[2] = 0.0 
        cosphi = np.dot(joint_vector, zero_vector)
        sinphi = la.norm(np.cross(joint_vector, zero_vector))
        #print sinphi, cosphi
        phi = np.arctan(sinphi/cosphi)
        if joint_vector[2] > 0:
            phi = -1 * phi    
        retval = result[link]
	    #print joint_n_prev
	    #print joint_n
	    #print (retval[0], retval[1], retval[2], phi)
        return (retval[0], retval[1], retval[2], phi)
	

    def rexarm_IK(self, pose):
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        """
        x, y, z, phi = pose 
        baseL = self.L[0]
        armL = self.L[1]
        tipL = self.L[3]
        if (np.sqrt(x**2 + y**2) - 2*armL - tipL) >0 or z<0:
            return None
        Bangle = np.arctan2(y,x)
        if Bangle > np.pi:
            Bangle = Bangle - 2 * np.pi
        R = np.sqrt(x**2 + y**2)
        tipZ = tipL * np.sin(phi)
        tipR = tipL * np.cos(phi)
        Rprime = R - tipR
        Zprime = z + tipZ - baseL
        M = np.sqrt(Rprime**2 + Zprime**2)
        if M >= 2*armL or (z+tipZ) < 0:
            return None
        Mangle = np.arccos((M/2.0)/armL)
        Eangle = 2 * Mangle
        Sangle = np.pi/2.0 - Mangle - np.arctan2(Zprime, Rprime)
        Wangle = np.pi/2.0 + phi - Eangle - Sangle
        if abs(Sangle) > self.clamp_values[1] or abs(Eangle) > self.clamp_values[2] or abs(Wangle) > self.clamp_values[3]:
            return None
        return [Bangle, Sangle, Eangle, Wangle]

    def rexarm_IK_Search(self, pose):
        """
        (x, y, z)
        """
        x, y, z = pose
        result = None
        for phi in xrange(90, -91, -1):
            result = self.rexarm_IK((x, y, z, phi*D2R))
            if result != None:
                #print phi*D2R
                return result
        for phi in xrange(90, 180, 1):
            result = self.rexarm_IK((x, y, z, phi*D2R))
            if result != None:
                #print phi*D2R
                return result
        return None

    def rexarm_collision_check(q):
        """
        Perform a collision check with the ground and the base
        takes a 4-tuple of joint angles q
        returns true if no collision occurs
        """
        pass 
