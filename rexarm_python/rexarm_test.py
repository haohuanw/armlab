#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rexarm import Rexarm
import numpy as np

if __name__ == "__main__":
    #waypoints = [(-80.0, 90.0, 350.0), (70.0, 80.0, 400.0), (-180.0, -250.0, 125.0),(240.0, -190.0, 95.0), (-80.0, 90.0, 350.0)]
    waypoints = []
    for t in xrange(0,12,1):
        x = 150 * np.cos(t)
        y = 150 * np.sin(t)
        z = 5.0 + 30.0 * t
        waypoints.append((x, y, z))

    rex = Rexarm()
    for w in waypoints:
        rex.joint_angles_fb = rex.rexarm_IK_Search(w)
        print rex.joint_angles_fb
        print rex.rexarm_FK(3)
