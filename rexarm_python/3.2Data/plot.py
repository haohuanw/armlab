import numpy as np
import csv

def readCSV(filename):
    ret = []
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            temp = [float(r) for r in row]
            ret.append(temp)
    return ret

def writeCSV(filename):
    arr = np.load(filename+'.npy')
    with open(filename+'.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile)
        for a in arr:
            writer.writerow(a)
    return arr

if __name__ == "__main__":
    # joint_fb = readCSV('jointfeedback.csv')
    # time_fb = readCSV('timefeedback.csv')
    # print joint_fb
    pos = writeCSV("waypointsPos")
    print pos
