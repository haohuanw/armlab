import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.currentFrame=np.array([])
	self.bwFrame = np.array([])
	self.camera_matirx = None
	self.dist_coefs = None
	self.new_camera_matrix = None
	self.calibration_loaded = False

        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 9                                     # Change!
        self.real_coord = np.float32([[200.0,200.0], [200.0,0.0], [200.0,-200.0],
                                      [-10.0,200.0] , [0.0,0.0] , [-10.0,-200.0],
                                      [-200.0,200.0],[-200.0,0.0],[-200.0,-200.0]])
	#self.real_coord = np.float32([[-200.0,200.0], [0.0,0.0], [200.0,200.0]])
        self.mouse_coord = np.float32([[0.0 for _ in xrange(2)] for __ in xrange(self.aff_npoints)])      
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32([[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,1.0]])
    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret==True):
            self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
	    self.bwFrame = cv2.cvtColor(self.currentFrame, cv2.COLOR_BGR2GRAY)
	if self.calibration_loaded:
	    self.currentFrame = cv2.undistort(self.currentFrame, self.camera_matrix, self.dist_coefs, None, self.new_camera_matrix) 
	    self.bwFrame = cv2.undistort(self.bwFrame, self.camera_matrix, self.dist_coefs, None, self.new_camera_matrix) 

    def compute_affine_matrix(self):
	A = np.float32([[0.0 for _ in xrange(6)] for __ in xrange(self.aff_npoints*2)])
	b = np.float32([0.0 for _ in xrange(self.aff_npoints*2)])
	for i in xrange(0,self.aff_npoints*2,2):
	    x = self.mouse_coord[i/2][0]
	    y = self.mouse_coord[i/2][1]
	    A[i][0] = A[i+1][3] = x
	    A[i][1] = A[i+1][4] = y
            A[i][2] = A[i+1][5] = 1
	    b[i] = self.real_coord[i/2][0] 
	    b[i+1] = self.real_coord[i/2][1]
	psudo_A = np.linalg.pinv(A)
	x = np.dot(psudo_A, b)
	self.aff_matrix[0][0] = x[0]
	self.aff_matrix[0][1] = x[1]
	self.aff_matrix[0][2] = x[2]
	self.aff_matrix[1][0] = x[3]
	self.aff_matrix[1][1] = x[4]
	self.aff_matrix[1][2] = x[5]

    def addTarget(self, target):
	self.currentFrame = cv2.circle(self.currentFrame, target, 10, 255, -1)

    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """
	self.calibration_loaded = True
	data = np.load("calibration.npz")
	self.camera_matrix = data["camera_matrix"]
	self.dist_coefs = data["dist_coefs"]
	h, w = self.currentFrame.shape[:2]
	self.new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix, self.dist_coefs, (w, h), 1, (w, h))
