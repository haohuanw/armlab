import numpy as np
import numpy.linalg as la
import copy

L = [115, 100, 100, 109]

#return square length of the angle point to
def law_of_cosine_length(L1, L2, angle):
    #print "length", L1, L2, angle
    return np.sqrt(L1**2 + L2**2 - 2*L1*L2*np.cos(angle))

#return cosine of angle between L1 and L2
def law_of_cosine_angle(L1, L2, L3):
    #print "angle: ", L1, L2, L3
    #print "cos: ",(-L3**2 + L1**2 + L2**2) / (2*L1*L2)
    return np.arccos((-L3**2 + L1**2 + L2**2) / (2*L1*L2))

def inv_kin(x, y, z, phi):
    # z is smaller than L[0]
    joint_angle = [0.0, 0.0, 0.0, 0.0]
    R = np.sqrt(x**2 + y**2)
    joint_angle[0] = np.arctan2(y, x) - np.pi/2.0
    d0joint3length = law_of_cosine_length(R, L[3], phi)
    d0joint3angle = law_of_cosine_angle(R, d0joint3length, L[3])
    if z > L[0]:
        d0_z = z - L[0]
        d0joint3angle_compl = np.pi/2.0 + d0joint3angle
    else:
        d0_z = L[0] - z
        d0joint3angle_compl = np.pi/2.0 - d0joint3angle

    M = law_of_cosine_length(d0_z, d0joint3length, d0joint3angle_compl)
    alpha1 = law_of_cosine_angle(L[1], M, L[2])
    alpha2 = law_of_cosine_angle(L[1], L[2], M)
    if z > L[0]:
        alpha1_compl = law_of_cosine_angle(M, d0joint3length, d0_z)
        joint_angle[1] = np.pi - alpha1 - alpha1_compl - d0joint3angle_compl
        joint_angle[2] = np.pi - alpha2
        joint1joint3length = np.sqrt(R**2 + d0_z**2)
        alpha3_compl = law_of_cosine_angle(M, L[3], joint1joint3length)
	joint_angle[3] = np.pi - (np.pi - alpha1 - alpha2) - alpha3_compl
    else:
        alpha1_compl = law_of_cosine_angle(d0_z, M, d0joint3length)
        joint_angle[1] = np.pi - alpha1 - alpha1_compl 
        joint_angle[2] = np.pi - alpha2
        joint_angle[3] = np.pi - (np.pi - alpha1 - alpha2) - (np.pi - alpha1_compl - d0joint3angle_compl) - (np.pi - phi - d0joint3angle)
    return joint_angle

def rexarm_FK(link, joint_angles_fb):
    alpha = [np.pi/2, 0, 0, 0]
    a = [0, L[1], L[2], L[3]]
    d = [L[0], 0, 0, 0]
    theta = [joint_angles_fb[0]-np.pi/2, joint_angles_fb[1]+np.pi/2, joint_angles_fb[2], joint_angles_fb[3]]
    mat_array = np.array([[1, 0, 0, 0],
    			 [0, 1, 0, 0],
    			 [0, 0, 1, 0],
    			 [0, 0, 0, 1]])	
    result = np.array(np.zeros((4, 4)))
    for i in xrange(4):
        A = np.array(
    	[[np.cos(theta[i]), -np.sin(theta[i])*np.cos(alpha[i]), np.sin(theta[i])*np.sin(alpha[i]), a[i]*np.cos(theta[i])],
             [np.sin(theta[i]), np.cos(theta[i])*np.cos(alpha[i]), -np.cos(theta[i])*np.sin(alpha[i]), a[i]*np.sin(theta[i])],
             [0            , np.sin(alpha[i])              ,  np.cos(alpha[i])              , d[i]              ],
             [0            , 0                          ,  0                          , 1                 ]]
    	)
        mat_array = np.dot(mat_array, A)	
        result[i] = mat_array[:,3]
    # calculate psi
    joint_n_prev = result[-2][:3]
    joint_n = result[-1][:3]
    joint_vector = np.subtract(joint_n, joint_n_prev)
    joint_vector = joint_vector / la.norm(joint_vector)
    zero_vector = copy.deepcopy(joint_vector)
    zero_vector[2] = 0.0 
    cosphi = np.dot(joint_vector, zero_vector)
    sinphi = la.norm(np.cross(joint_vector, zero_vector))
    #print sinphi, cosphi
    phi = np.arctan(sinphi/cosphi)
    retval = result[link]
    return (retval[0], retval[1], retval[2], phi)


if __name__ == "__main__":
    inv_res = inv_kin(-80.0, 90.0, 400.0, 0.0)
    #for res in inv_res:
    #    print res*180.0/np.pi
    print "(x, y, z, phi)", rexarm_FK(3, inv_res)
